import java.util.Date;

/*
* @file orang
* @auothor Yafi Hudatama Wibowo
* @created 1 April 2020 pk 16.57
*/

public class orang {
    protected String nama;
    protected Date tanggalLahir;
    
    public void setNama(String nm){
        nama = nm ;
    }
    
    public void setTglLahir(Date t){
        tanggalLahir = t;
    }
    
    public String getNama(){
        return nama;
    }
    
    public Date getTglLahir(){
        return tanggalLahir;
    }
}
