import java.util.Date;

public class Dokter extends orang {
    private Date tanggalMulai;
    private String spesialisasi;
    
    public void setTglMulai(Date t){
        tanggalMulai = t;
    }
    
    public void setSpec(String s){
        spesialisasi = s;
    }
    
    public Date getTglMulai(){
        return tanggalMulai;
    }
    public String getSpec(){
        return spesialisasi;
    }
    
    public static void main(String[] args){
        Dokter doc = new Dokter();
        doc.setNama("Yafi");
        doc.setTglLahir(new Date(12, 2, 2001));
        doc.setTglMulai(new Date(12, 2, 2025));
        doc.setSpec("Dokter Kandungan");
        
        System.out.println("Data Dokter Sebagai Berikut");
        System.out.println("Nama : "+ doc.getNama());
        System.out.println("Tanggal Lahir : "+doc.getTglLahir());
        System.out.println("Tanggal Mulai : "+doc.getTglMulai());
        System.out.println("spesialisasi : "+doc.getSpec());
    }
}
