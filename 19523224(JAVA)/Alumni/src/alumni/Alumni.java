package alumni;

public class Alumni {
    private String nama;
    private char gender;
    private int TahunLulus;
    private int tahunLulus;
    
    public void setNama(String nama){
        this.nama = nama;
    }
    public void setGender(char gender){
        this.gender = gender;
    }
    public void setTahunLulus(int tahunLulus){
        if(tahunLulus>=1948 && tahunLulus<=2014){
        this.tahunLulus = tahunLulus;
        }
    }
    public String getnama(){
        return this.nama;
    }
    public String getGender(){
     if(this.gender == 'L'){   
         return "Laki-Laki";
    } else{
         return "Perempuan";
     }
    }
    public int getTahunLulus(){
        return this.tahunLulus;
    }
    
    public static void main(String[] args) {
        Alumni al = new Alumni();
        System.out.println("Data Alumni Disimpam");
        al.setNama("Fadil Indra Sanjaya");
        al.setGender('L');
        al.setTahunLulus(2013);
        System.out.println();
        
        System.out.println("Data Alumni Ditampilkan");
        System.out.println("Nama :"+al.getnama());
        System.out.println("Gender :"+al.getGender());
        System.out.println("TahunLulus :"+al.getTahunLulus());
        
    }
    
}
