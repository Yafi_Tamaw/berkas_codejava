public class Penjual {

    private String nama;
    private int umur;
    private sayur sayur;

    public Penjual(String n, int u) {
        nama = n;
        umur = u;
    }
    public void setSayur(sayur i) {
        sayur = i;
    }

    public void DisplayPenjual() {
        System.out.println("Penjual bernama "+ nama+ " berumur "+ umur + " tahun");
        System.out.println("Menjual sayuran berupa "+sayur); 
    }
}
