    import java.util.Scanner;
public class Hewan {
    
        // main method utama 
        public static void main(String[]agrs){
            // deklarasi variabel
            int umur;                   
            String nama = "Bambang"; 
            String suara =  "Mouu";
            int bulanLahir;
            
            // input data
            Scanner sc = new Scanner (System.in);
            umur = sc.nextInt();
            
            System.out.println("Usia Sapi : ");
            // percabangan 
            if(umur>5){ // kondisi
                System.out.println("dewasa");
            } else if(umur>3 && umur<5){
                System.out.println("Remaja");
            } else {
                System.out.println("anak-anak");
            }
            
            System.out.println("Bulan lahir sapi : ");
            // input masukan
            bulanLahir = sc.nextInt();
            switch(bulanLahir){ // kondisi
                case 1 : 
                    System.out.println("Januari");
                    break;
                case 2:
                    System.out.println("Februari");
                    break;
                default:
                    System.out.println("Bulan lainnya");
                    break;
            }
            System.out.println("Suara sapi :");
            // perulangan lopp for
            for(int i = 0; i<4; i++){ 
                System.out.println(suara);
            }
            // awalan kondisi banyaknya sapi
            int tambahSapi = 1;
            // perulangan loop while
            while(tambahSapi<=10){
                System.out.println("sapi"+ tambahSapi);
                tambahSapi++;
            }
            System.out.println("Masukan Array sapi");
            int banyak  = 4; // deklarasi variabel dengan inialisasi
            String [] namaSapi = new String[banyak];
            
            // perulangan lopp for 
            for (int i=0;i<banyak;i++){
                System.out.println("Masukan nama sapi : ");
                namaSapi[i] = sc.nextLine(); // iput data kedalam array
               }
            System.out.println("Keluaran/Isi Array : ");
            // perulangan lopp for
            for(int i=0;i<namaSapi.length;i++){
                System.out.println(namaSapi[i]);
        }
            System.out.println("Contoh array lainnya : ");
            // perulanga for bersarang
            int[][] a = new int[2][3];
            for(int i =0; i<2; i++){
                for(int j = 0; j<3; j++){
                    a[i][j]= sc.nextInt(); // data disimpan di dalam array
                }
            }
            System.out.println("Kelauran/isi array 2x3");
            // perulangan for bersarang
            for(int i=0; i<2; i++){
                for(int j=0; j<3; j++){
                    System.out.println(a[i][j]); // input data ke dalam array
                }
                System.out.println(" "); // spasi 
            }
           }
        }