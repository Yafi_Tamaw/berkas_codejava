/**
 * @Nama : Yafi Hudatama Wibowo
 * @Nim : 19523224
 * created rabu pk 15.28 
 */
public class Sekolah {
    String namaSekolah;
    String alamatSekolah;
    String tingkatSekolah;
    
    public Sekolah(String Ns, String Al, String ts){
        this.namaSekolah = Ns;
        this.alamatSekolah = Al;
        this.tingkatSekolah = ts; 
    }

    public void setNamaSekolah(String ns){
        namaSekolah = ns;
    }
    public void setAlamat(String al){
        alamatSekolah = al;
    }
    public void setTingkat(String ts){
       tingkatSekolah = ts;
    }
    public String getNama(){
        return namaSekolah;
    }
    public String getAlamat(){
        return alamatSekolah;
    }
    public String getTingkat(){
        return tingkatSekolah;
    }
   public String toString(){
        return "Nama Sekolah "+namaSekolah+"beralamat"+alamatSekolah+"Tingkat Sekolah"+ tingkatSekolah;
        }
    }

