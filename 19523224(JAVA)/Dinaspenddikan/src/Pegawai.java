
/**
 * @nama : Yafi Hudatama Wibowo
 * @nim : 19523224
 * @created : Rabu pk 15.58 
 */
public class Pegawai {
    protected String nama;
    Sekolah sekolah;
    String Pegawai;
    
    public Pegawai(String n){
        this.Pegawai = n;
    }
    public void setNama(String n){
        this.nama = n;
    }
    public String getNama(){
        return nama;
    }
    public void setSekolah(Sekolah s){
        this.sekolah = s;
    }
    public Sekolah getSekolah(){
        return sekolah;
    }
}
