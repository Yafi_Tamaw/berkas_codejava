/**
 * @nama : Yafi Hudaatama Wibowo
 * @nim : 19523224
 * @created : Rabu pk 16.20
 */
public class stafAdministrasi {
    String bagian;
    String nama;
    
    stafAdministrasi(String n, String b){
        this.nama = n;
        this.bagian = b;
    }
    public void setBagian(String b){
        this.bagian = b;
    }
    public String getBagian(){
        return bagian;
    }
}
