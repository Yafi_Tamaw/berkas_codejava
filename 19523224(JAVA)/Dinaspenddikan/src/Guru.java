
/**
 * @Nama : Yafi Hudatama Wibowo
 * @Nim : 19523224
 * @created : Rabu pk 15.50
 */
public class Guru {
    String mapel;
    String nama;

   public Guru(String nama, String mapel){
        this.nama = nama;
        this.mapel = mapel;
    }
   public void setMapel(String m){
       this.mapel = m;
   }
   public String getMapel(){
       return mapel;
   }
}
