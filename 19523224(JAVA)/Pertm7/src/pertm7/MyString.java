package pertm7;
import java.lang.String;
/**
 * Nama File : MyString.Java
 * Author : Arya Nanda M.p
 * Created : 18 Maret 2020 
 */
public class MyString {
     String str;
    int max;
    
    public void setMax(int m){
            this.max = m;
    }
    
    public void setString(String s){
           if( s.length() < max){
               System.out.println("String telah disimpan");
           }else{
               this.str = s;
               System.out.println("String tidak bisa disimpan");
        }
    }
      public String printString(){
           str = "Informatika";
          return str;
       }

        public static void main(String[] agrs){
        MyString ms = new MyString();
        System.out.println("Set Maksimum = 8 ");
        ms.setMax(8);
        ms.setString("Informatika");
        System.out.println();
        
        System.out.println("Set Maksimum = 20 ");
        ms.setMax(20);  
        ms.setString("Informatika");
        System.out.println("isinya : "+ ms.printString());
    }
}
