package perulangandasar;

/**
 * Nama file : perulangandasar
 * Author : Yafi Hudatama Wibowo
 * Created : 25 Maret 2020
 */
public class PerulanganDasar {
            int i;
    public static void main(String[] args) {
        // TODO code application logic here
        PerulanganDasar d1= new PerulanganDasar();
            d1.i = 1;
            PerulanganDasar d2 = new PerulanganDasar();
            d1.i = 2;
            PerulanganDasar d3 = new PerulanganDasar();
            d1.i = 3;
            PerulanganDasar d4 = new PerulanganDasar();
            
            d2 = d3;
            d3 = d2;
            d1 = d4;
            d2 = d1;
            
            System.out.println(d1.i);
            System.out.println(d2.i);
            System.out.println(d3.i);
            System.out.println(d4.i);
    }
    
}
