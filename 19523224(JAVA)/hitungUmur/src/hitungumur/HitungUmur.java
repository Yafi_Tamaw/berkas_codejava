package hitungumur;

import java.util.Scanner;

public class HitungUmur {
    private int age;	
    private int initial;
	public HitungUmur(int initialAge) {
  		// Add some more code to run some checks on initialAge
            if(initial<0){ // initial age is invalid 
              System.out.println("Age is no valid, setting age to 0.");
              this.age = 0; 
          }
          else{
              this.age = initialAge;
          }
    }

	public void amIOld() {
  		// Write code determining if this person's age is old and print the correct statement:
          String result = " ";
          if(age>18){
                result = "You are old.";
	        }
            else if(age>=13){
                        result = "You are a teenager.";
                 }
                 else{
                    result = "you are young.";
             }
             System.out.println(result);
    }

	public void yearPasses() {
  		// Increment this person's age.
          this.age++;
	}
}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for (int i = 0; i < T; i++) {
			int age = sc.nextInt();
			HitungUmur p = new HitungUmur(age);
			p.amIOld();
			for (int j = 0; j < ; j++) {
				p.yearPasses();
			}
			p.amIOld();
			System.out.println();
        }
		sc.close();
    }
}