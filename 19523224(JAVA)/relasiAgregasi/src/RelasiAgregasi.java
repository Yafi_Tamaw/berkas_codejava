/**
 * @file Agregasi.java
 * @author Yafi Hudatama Wibowo
 * @Created 27 Maret 2020 pk 21.25
 */
public class RelasiAgregasi {
    public static void main(String[] args) {
        Penjual p = new Penjual("Bambang", 25);
        Sayuran kangkung = new Sayuran("Kangkung",2);
        p.setSayuran(kangkung);
        p.DisplayPenjual();   
    }
    
}
