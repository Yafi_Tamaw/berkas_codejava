public class Penjual {

    private String nama;
    private int usia;
    private Sayuran Sayuran;

    public Penjual(String n, int u) {
        nama = n;
        usia = u;
    }
    public void setSayuran(Sayuran i) {
        Sayuran = i;
    }
    public void DisplayPenjual() {
        System.out.println("Penjual dengan nama "+ nama +"Berusia "+usia);
        System.out.println("Menjual berupa "+ Sayuran +" hari");
    }
}
