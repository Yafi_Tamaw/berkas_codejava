import java.util.Date;
public class Orang {
    private String nama;
    private int umur;
    private Date tanggalLahir;
    private Date harga;
    private String lok;
    
    public void setNama(String nm){
        nama = nm;
    }
    public void setUmur(int u){
        umur = u;
    }
    public void setTglLahir(Date t){
        tanggalLahir = t;
    }
    public void setHarga(Date h){
        harga = h;
    }
    public void setLok(String L){
        lok = L;
    }
    public String getNama(){
        return nama;
    }
    public int getUmur(){
        return umur;
    }
    public Date getTglLahir(){
        return tanggalLahir;
    }
    public Date getHarga(){
        return harga;
    }
    public String getLok(){
        return lok;
    }
    
}
