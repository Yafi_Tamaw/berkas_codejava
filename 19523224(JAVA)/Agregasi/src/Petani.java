/*
 * @file Petani.java
 * @auothor Yafi Hudatama Wibowo
 * created : 27 Maret 2020 pk : 20.37
 */

public class Petani {

    private String nama;
    private int umur;
    private Sayur Sayur;

    public Petani(String n, int u) {
        nama = n;
        umur = u;
    }
    public void setSayur(Sayur i) {
        Sayur = i;
    }
    public void Dislaypetani() {
        System.out.println("Petani dengan nama "+ nama +"umur " + umur);
        System.out.println("Menjual berupa :"+ Sayur);
    }
}
