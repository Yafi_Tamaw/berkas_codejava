/*
 * @file Sayur.java
 * @auothor Yafi Hudatama Wibowo
 * created : 27 Maret 2020 pk : 20.37
 */

public class Sayur {
    private String nama;
    private int usia;

    public Sayur(String n, int u) {
            nama = n;
            usia = u;
    }
    public String toString() {
        return "Sayur " +nama+ "berusia "+ usia;
    }
}
