package mathroff;
import java.util.Scanner;
/**
 * nama file : mathrooff
 * Author : Yafi Hudatama Wibowo
 * Creted : 20 Maret 2020
 */
public class MathRoff {
    public static void main(String[] args) {
        // TODO code application logic here
            Scanner sc = new Scanner (System.in);
            int bil = sc.nextInt();
            int akar = (int) Math.sqrt(bil);
            int S1 = Math.abs(bil - akar*akar);
            int S2 = Math.abs(bil - (akar+1)*(akar+1));
            
            if(S1<S2){
                System.out.println(akar);
            } else {
                System.out.println(akar+1);
            }
    }
    
}
